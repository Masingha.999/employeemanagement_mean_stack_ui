import { Component, OnInit } from '@angular/core';
import { TokenStoreService } from '../services/token-store.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  currentUser: any;

  constructor(private token: TokenStoreService) { }

  ngOnInit() {
    this.currentUser = this.token.getUser();
  }

}
