import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Observable } from "rxjs";

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  totalSE!: Observable<any>;
  totalQA!: Observable<any>;
  totalHR!: Observable<any>;
  totalMark!: Observable<any>;

  constructor(private employeeService : EmployeeService) { }

  ngOnInit() {
    this.reloadData1();
    this.reloadData2();
    this.reloadData3();
    this.reloadData4();
  }

  reloadData1(){
    this.employeeService.getSECount().subscribe(data =>{
      this.totalSE = data;
    });
 }

 reloadData2(){
  this.employeeService.getQACount().subscribe(data =>{
    this.totalQA = data;
  });
}

reloadData3(){
  this.employeeService.getHRCount().subscribe(data =>{
    this.totalHR = data;
  });
}

reloadData4(){
  this.employeeService.getMarkCount().subscribe(data =>{
    this.totalMark = data;
  });
}

}
