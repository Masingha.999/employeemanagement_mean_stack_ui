import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { EmployeeDetailsComponent } from '../employee-details/employee-details.component';
import { UpdateEmployeeComponent } from '../update-employee/update-employee.component';
import { Observable } from "rxjs";
import { EmployeeService } from '../employee.service';
import {  Employee } from '../employee';
import { Router } from '@angular/router';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees!: Observable<Employee[]>;

  constructor(private employeeService : EmployeeService ,
  private router: Router) {
    
   }

  ngOnInit() {
    this.reloadData();
  }

  reloadData(){
    this.employees = this.employeeService.getEmployeesList();
  }
  
  deleteEmployee (id: String){
    this.employeeService.deleteEmployee(id).subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error));

  }

  employeeDetails(id: String){
    this.router.navigate(['details' , id]);
  }

  updateEmployee(id: String){
    this.router.navigate(['update' , id]);
  }

}
