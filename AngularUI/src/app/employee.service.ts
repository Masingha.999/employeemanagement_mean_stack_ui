import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseUrl = 'http://localhost:8080/employees';

  constructor(private http: HttpClient) { }

  getEmployee(_id: String): Observable <any> {
    return this.http.get(`${this.baseUrl}/emp/${_id}`);

  }

  createEmployee(employee: Object): Observable <Object> {
    return this.http.post(`${this.baseUrl}`, employee);
  }

  updateEmployee(_id: String, value: any): Observable <Object> {
    return this.http.put(`${this.baseUrl}/emp/${_id}`, value);
  }

  deleteEmployee(_id: String): Observable <any>{
    return this.http.delete(`${this.baseUrl}/emp/${_id}`, { responseType: 'text' });
  }

  getEmployeesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  getSECount(): Observable<any>{
     return this.http.get(`${this.baseUrl}/countSE`);
  }

  getQACount(): Observable<any>{
     return this.http.get(`${this.baseUrl}/countQA`);
  }

  getHRCount(): Observable<any>{
    return this.http.get(`${this.baseUrl}/countHR`);
 }

 getMarkCount(): Observable<any>{
    return this.http.get(`${this.baseUrl}/countMark`);
 }



}
