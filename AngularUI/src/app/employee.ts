export class Employee {
    _id!: String;
    name!: String;
    position!: String;
    salary!: Number; 
    department!: String;
    joinDate!: Date;

}