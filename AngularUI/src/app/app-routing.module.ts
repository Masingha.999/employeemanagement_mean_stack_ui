import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { CartUserComponent } from './cart-user/cart-user.component';
import { CartAdminComponent } from './cart-admin/cart-admin.component';

import {EmployeeDetailsComponent } from './employee-details/employee-details.component';
import {CreateEmployeeComponent } from './create-employee/create-employee.component';
import {EmployeeListComponent} from './employee-list/employee-list.component';
import {UpdateEmployeeComponent} from './update-employee/update-employee.component';
import {DepartmentListComponent} from './department-list/department-list.component';

const routes: Routes = [
  
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: CartUserComponent },
  { path: 'admin', component: CartAdminComponent },
  
  {path: 'employees', component: EmployeeListComponent },
  {path: 'department-List', component: DepartmentListComponent},
  {path: 'add', component: CreateEmployeeComponent},
  {path: 'update/:id', component: UpdateEmployeeComponent},
  {path: 'details/:id', component: EmployeeDetailsComponent},

  { path: '', redirectTo: 'employees', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
