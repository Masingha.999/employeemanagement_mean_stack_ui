import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  // websiteList: any = ['Software Engineering', 'QA', 'HR Management', 'Marketing Management']

  // form = new FormGroup({
  //   name: new FormControl('', Validators.required)
  // });
   
  // get f(){
  //   return this.form.controls;
  // }
   
  // submit(){
    
  // }
   


  employee: Employee = new Employee();
  submitted = false;

  constructor(private employeeService: EmployeeService, 
    private router: Router) { }

  ngOnInit(){

  }

  // form = new FormGroup({
  //   name: new FormControl('', Validators.required)
  // });

  // get f(){
  //   return this.form.controls;
  // }

  newEmployee(): void{
    this.submitted = false;
    this.employee = new Employee();
  }

  save(){
    this.employeeService.createEmployee(this.employee).subscribe(data => {
      console.log(data)
      this.employee = new Employee();
      this.gotoList();
    },
    error => console.log(error));

  }

  onSubmit(){
    this.submitted = true;
    this.save();
    // console.log(this.form.value);
  }

  gotoList(){
    this.router.navigate(['/employees']);
  }

}
